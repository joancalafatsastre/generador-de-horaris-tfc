Explicació extensa
=====================

Es tracta d'una aplicació que utilitza l'encarregat/da d'una empresa per els horaris del seus empleats.

L'aplicació gestiona varies tendes, establiments i departaments de l'empresa.

L'administrador podrà crear torn manualment o escollir uns quants predeterminats.

Es podrà imprimir els horaris d'un departament concret i d'una tenda concreta.

De cada empleat ens dirà:

	- el contracte que té (40h, 30h,20h, etc...)
	- els dies lliure que li toquen
	- els dies lliure que porta i que li queden
	- si treballa sols els caps de setmana
	
Que l'aplicació et pugui orientar prèvia introducció de dades sobre quants
empleats es necessiten en cada moment del dia:

Exemple:
Si una caixera s'estima que (per hora, dia o setmana) ha de fer de caixa .... X €
Se sap el que fa cada botiga per dia o per franja horària.


Casos d'ús previstos / Histories d'usuari
=========================================

	- L'encarregat escull l'establiment i el departament que vol dins de l'establiment anterior
	- L'encarregat cerca un empleat i li surt un històric de les hores que porta en cada setmana, cada mes i cada any, els dies lliures que porta, etc, etc
	- Imprimeix l'horari de la setmana que esculli
	- L'encarregat afegeix els horaris de cada empleat durant la setmana escollida
	- L'encarregat afegeix un establiment, un departament dins d'un establiment o un empleat
	 
	

Tecnologies associades previstes
================================

URL del wireframe https://app.moqups.com/a15aracarjim@iam.cat/1XSNlJd28l/view

