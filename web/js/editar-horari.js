$(document).ready(init);

function init() {
	console.log('editar-horari.js');
	let diaInici = $("#data").text();
	console.log("INICI: " + diaInici);
	let horariId = $('#horari-id').text();
	console.log("ID HORARI: " + horariId);
	let numEmpleats = calculaDates(diaInici);
	let mesAny = $('#data').text().substring(2);
	$(".form-dia").each(function(i, obj) {
		let dni = $(this).parent().attr('id').substring(8);
		let dia = $(this).attr('id').substring(4) + mesAny;
		console.log('dni ' + dni);
		console.log('dia ' + dia);
		mostraForm(horariId, dia, dni);
	});
	$("#editar-horari").on("click", function() {
		//console.log("click edit");
		let url = '../veure-horari/' + horariId;
		guardarDades(numEmpleats, url);
	});
	$('#editar-imprimir-horari').on("click", function() {
		let url = '../imprimir-horari/' + horariId;
		guardarDades(numEmpleats, url);
	});
}

function guardarDades(numEmpleats, url) {
	$('#button-form').empty().html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>');
	let count = 0;
	$("form").each(function(i, obj) {
		$.post($(this).attr("action"), $(this).serialize(), function () {
        	   	console.log("form submited");
		})
			.always(function() {
				count++;
				if (count == numEmpleats * 7 + 1) {
					location.href = url;
				}
			});
	});
}

function calculaDates(diaInici) {
	let cont = 0;
	let numEmpleats = 0;
	let dia = parseInt(diaInici.substring(0,2));
	let mes = parseInt(diaInici.substring(3,5));
	let any = parseInt(diaInici.substring(6,11));
	let data = new Date(any, (mes-1), dia);
	console.log("DATA: " + data);
	$("th").each(function(i, obj) {
		if (i > 0) {
			let nouDia = new Date(data);
			nouDia.setDate(data.getDate()+cont++);
			$(this).append(" " + nouDia.getDate());
		}
	});
	cont = 0;
	$(".form-dia").each(function(i, obj) {
		let nouDia = new Date(data);
		nouDia.setDate(data.getDate()+cont++);
		let dia = ("0" + nouDia.getDate()).slice(-2);
		$(this).attr("id", "dia-" + dia);
		if (cont > 6) {
			numEmpleats++;
			cont = 0;
		}
	});
	return numEmpleats;
}

function mostraForm(horariId, dia, dni) {
	console.log("mostraForm " + horariId + " " + dia + " " + dni);
	$.ajax({
		async: true,
		type: 'POST',
		url: '../editar-dia-empleat/' + horariId + '/' + dia + '/' + dni + '/',
		dataType: 'html',
		success: function (data) {
			$('#empleat-' + dni).find('td#dia-' + dia.slice(0,2)).html(data);
		},
		error: function (data) {
			console.log("ERROR!");
		}
	});
	return false;
}
