$(document).ready(init);

function init() {
	console.log('veure-horari.js');
	let diaInici = $("#data").text();
	console.log("INICI: " + diaInici);
	let horariId = $('#horari-id').text();
	console.log("ID HORARI: " + horariId);
	let numEmpleats = calculaDates(diaInici);
	let len = $(".form-dia").length;
	$(".form-dia").each(function(i, obj) {
		let dni = $(this).parent().attr('id').substring(8);
		let dia = $(this).attr('id').substring(4);
		console.log('dni ' + dni);
		console.log('dia ' + dia);
		mostraForm(horariId, dia, dni);
		if (i+1 == len){
			$("#imprimir-horari").attr('disabled', false);
			// check param and print
			let urlParams = new URLSearchParams(window.location.search);
			if (urlParams.has('action') && urlParams.get('action') == 'print') {
				imprimir();
			}
		}
	});
	$("#imprimir-horari").on("click", function() {
		imprimir();
	});
}

function imprimir() {
	console.log("imprimir horari");
	let header = $('h1').text() + " /// " + $('h2').text();
	let departament = $('h1').text();
	let data = $('h2').text();
	let th = new Array();
	$("th").each(function(i, obj) {
		if (i < 8) {
			th.push($(this).text());
		}
	});
	console.log(th);
	let empleats = new Array();
	let tr = new Array();
	let rowCount = 0;
	tr[rowCount] = new Array()
	$("td.nom-empleat").each(function(i, obj) {
		empleats.push($(this).text());
	});
	console.log(empleats);
	$("td.form-dia").each(function(i, obj) {
		if (i == (7 * (rowCount + 1))) {
			tr[++rowCount] = new Array();
		}
		tr[rowCount].push($(this).text());
		//console.log(rowCount + " -> " + $(this).text());
	});
	console.log(tr);

	let externalDataRetrievedFromServer = new Array();
	for (let i = 0; i < tr.length; i++) {
		let tempArr = new Array();
		tempArr[th[0]] = empleats[i];
		for (let j = 0; j < tr[i].length; j++) {
			tempArr[th[j+1]] = tr[i][j];
		}
		externalDataRetrievedFromServer.push(tempArr);
	}

	console.log(externalDataRetrievedFromServer);

	function buildTableBody(data, columns) {
		var body = [];
		body.push(columns);
		data.forEach(function(row) {
			var dataRow = [];
			columns.forEach(function(column) {
				dataRow.push(row[column].toString());
			})
			body.push(dataRow);
		});
		return body;
	}

	function table(data, columns) {
		return {
			table: {
				headerRows: 1,
				widths: [ 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto' ],
				body: buildTableBody(data, columns)
			}
		};
	}

	let docDefinition = {
		pageSize: 'A4',
		pageOrientation: 'landscape',
		header: header,
		content: [
			{text: departament, style: 'header'},
			{text: data, style: 'header'},
			table(externalDataRetrievedFromServer, [th[0], th[1], th[2], th[3], th[4], th[5], th[6], th[7]])
		],
		styles: {
			header: {
				fontSize: 22,
       			bold: true,
				alignment: 'center'
			}
		}
	}

	pdfMake.createPdf(docDefinition).open();
}

function calculaDates(diaInici) {
	let cont = 0;
	let numEmpleats = 0;
	let dia = parseInt(diaInici.substring(0,2));
	let mes = parseInt(diaInici.substring(3,5));
	let any = parseInt(diaInici.substring(6,11));
	let data = new Date(any, (mes-1), dia);
	console.log("DATA: " + data);
	$("th").each(function(i, obj) {
		if (i > 0) {
			let nouDia = new Date(data);
			nouDia.setDate(data.getDate()+cont++);
			$(this).append(" " + nouDia.getDate());
		}
	});
	cont = 0;
	$(".form-dia").each(function(i, obj) {
		let nouDia = new Date(data);
		nouDia.setDate(data.getDate()+cont++);
		$(this).attr("id", "dia-" + nouDia.getDate());
		if (cont > 6) {
			numEmpleats++;
			cont = 0;
		}
	});
	return numEmpleats;
}

function mostraForm(horariId, dia, dni) {
	console.log("mostraForm " + horariId + dia + " " + dni);
	$.ajax({
		async: true,
		type: 'POST',
		url: '../veure-dia-empleat/' + horariId + '/' + dia + '/' + dni + '/',
		dataType: 'html',
		success: function (data) {
			$('#empleat-' + dni).find('td#dia-' + dia).html(data);
		},
		error: function (data) {
			console.log("ERROR!");
		}
	});
	return false;
}
