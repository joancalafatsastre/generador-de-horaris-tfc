<?php
// src/AppBundle/Repository/HorariRepository.php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class HorariRepository extends EntityRepository
{
	public function findAllOrderedById()
	{
		return $this
		->createQueryBuilder('h')
		->orderBy('h.id','DESC')
		->setMaxResults(4)
		->getQuery()
		->getResult();
	}

}
