<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


use AppBundle\Entity\Establiment;

class EstablimentController extends Controller
{

	/**
	 * @Route("/establiments", name="establiments")
	 */
	public function listAction(Request $request)
	{
		// Recupera tots els establiments
		$establiments = $this->getDoctrine()
			->getRepository('AppBundle:Establiment')
			->findAll();

		// Si no hi ha establiments
		if (count($establiments)==0) {
			return $this->render('establiments/list.html.twig', array(
				'title' => 'No hi ha Establiments.',
				'establiments' => null,
			));
		}
		
		// Si hi ha establiments
		return $this->render('establiments/list.html.twig', array(
			'title' => 'Establiments',
			'establiments' => $establiments,
		));
	}

	/**
	 * @Route("/nou-establiment", name="crearEstabliment")
	 */
	public function insertAction(Request $request)
	{
		$establiment = new Establiment();

		$form = $this->createFormBuilder($establiment)
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('save', SubmitType::class, array('label' => 'Crea',
				'attr' => [
					'class' => 'btn btn-primary',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($establiment);
			$em->flush();
			return $this->redirectToRoute('establiments');
		}

		return $this->render('establiments/form.html.twig', array(
			'message' => null,
			'title' => 'Crear Establiment',
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/editar-establiment/{id}", name="editarEstabliment")
	 */
	public function editAction($id, Request $request)
	{
		// Es recupera l'establiment a editar
		$em = $this->getDoctrine()->getManager();
		$establiment = $em->getRepository('AppBundle:Establiment')
			->findOneById($id);

		// Es crea el formulari
		$form = $this->createFormBuilder($establiment) 
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('save', SubmitType::class, array('label' => 'Editar',
				'attr' => [
					'class' => 'btn btn-primary',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
			return $this->redirectToRoute('establiments');
		}

		return $this->render('establiments/form.html.twig', array(
			'message' => null,
			'title' => 'Editar Establiment',
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/eliminar-establiment/{id}", name="eliminarEstabliment")
	 */
	public function removeAction($id, Request $request)
	{
		$entityManager = $this->getDoctrine()->getManager();
    $establiment = $entityManager->getRepository(Establiment::class)->find($id);

    if (!$establiment) {
        throw $this->createNotFoundException(
            "No s'ha trobat cap establiment amb l'id".$id
        );
    }
    $valorActivat= $establiment->getactivat();
    if($valorActivat==true){
    	$establiment->setactivat(0);
    	$entityManager->flush();
    }


    $em = $this->getDoctrine()->getManager();
	$connection = $em->getConnection();
	$statement = $connection->prepare("UPDATE Departament SET activat = 0
WHERE establiment = $id;");
	$statement->execute();
    return $this->redirectToRoute('establiments');
}
}
