<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Establiment;
use AppBundle\Entity\Departament;

class DepartamentController extends Controller
{

	/**
	 * @Route("/departaments", name="departaments")
	 */
	public function listAction(Request $request)
	{
		// Recupera tots els departaments
		$departaments = $this->getDoctrine()
			->getRepository('AppBundle:Departament')
			->findAll();

		// Si no hi ha departaments
		if (count($departaments)==0) {
			return $this->render('departaments/list.html.twig', array(
				'message' => 'No hi ha departaments.',
				'departaments' => null,
			));
		}
		
		// Si hi ha departaments
		return $this->render('departaments/list.html.twig', array(
			'title' => 'Departaments',
			'departaments' => $departaments,
		));


	}


	/**
	 * @Route("/nou-departament", name="crearDepartament")
	 */
	public function insertAction(Request $request)
	{
		$departament = new Departament();

		$form = $this->createFormBuilder($departament)
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('establiment', EntityType::class, array('class' => 'AppBundle:Establiment', 
					'choice_label' => 'nom',
					'attr' => [
						'class' => 'form-control',
						]))
			->add('save', SubmitType::class, array('label' => 'Crea',
				'attr' => [
					'class' => 'btn btn-primary',
					'multiple' => '',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($departament);
			$em->flush();
			return $this->redirectToRoute('departaments');

		}

		return $this->render('departaments/form.html.twig', array(
			'title' => 'Crear Departament',
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/editar-departament/{id}", name="editarDepartament")
	 */
	public function editAction($id, Request $request){
		// Es recupera la id del departament a editar
		$em = $this->GetDoctrine()->getManager();
		$departament = $em->getRepository('AppBundle:Departament')
			->findOneById($id);

		// Es crea el formulari

		$form = $this->createFormBuilder($departament)
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('establiment', EntityType::class, array('class' => 'AppBundle:Establiment', 
					'choice_label' => 'nom',
					'attr' => [
						'class' => 'form-control',
						]))
			->add('save', SubmitType::class, array('label' => 'Editar',
				'attr' => [
					'class' => 'btn btn-primary',
					'multiple' => '',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
			return $this->redirectToRoute('departaments');
		}

		return $this->render('departaments/form.html.twig', array(
			'title' => 'Editar Departament',
			'form' => $form->createView(),
		));


	}

	/**
	 * @Route("/deshabilitarDepartament/{id}", name="deshabilitarDepartament")
	 */
	public function deshabilitarAction($id)
	{
    $entityManager = $this->getDoctrine()->getManager();
    $departament = $entityManager->getRepository(Departament::class)->find($id);

    if (!$departament) {
        throw $this->createNotFoundException(
            "No s'ha trobat cap departament amb l'id".$id
        );
    }
    $valorActivat= $departament->getactivat();
    if($valorActivat==true){
    	$departament->setactivat(0);
    	$entityManager->flush();
    }else{
    	$departament->setactivat(1);
    	$entityManager->flush();
    }
    return $this->redirectToRoute('departaments');
}

}
