<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
		$session = $this->get('session');
		$session->remove('idDepartament');
		$session->remove('idData');
		
        $horaris = $this->getDoctrine()
					->getRepository('AppBundle:Horari')
					->findAllOrderedById();
		
        // Si no hi ha horaris
        if (count($horaris)==0) {
            return $this->render('default/index.html.twig', array(
                'message' => "Benvingut al teu creador d'horaris",
                'horaris' => null,
            ));
        }
        // Si hi ha horaris
        return $this->render('default/index.html.twig', array(
            'message' => null,
            'horaris' => $horaris,
            ));
        
    }

	/**
     * @Route("/info", name="infoTest")
     */
	public function infoTest(Request $request)
	{	
		$session = $this->get('session');
		$dep = $session->get('idDepartament');
		$data = $session->get('idData');


		return new Response(
            '<html><body><p>ID Departament: '.$dep.'</p><p>Data: '.$data.'</p></body></html>'
        );
	}
}
