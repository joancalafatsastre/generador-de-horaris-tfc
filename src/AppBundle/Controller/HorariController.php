<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Horari;
use AppBundle\Entity\Empleat;
use AppBundle\Entity\HorariEmpleat;
use AppBundle\Entity\TipusHora;
use AppBundle\Entity\Establiment;
use AppBundle\Entity\Departament;

use Doctrine\ORM\EntityRepository;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints\DateTime;

class HorariController extends Controller
{
	/**
	 * @Route("/horaris", name="horaris")
	 */
	public function listAction(Request $request)
	{
		
		// Recupera tots els horaris
		$horaris = $this->getDoctrine()
			->getRepository('AppBundle:Horari')
			->findAll();

		// Si no hi ha horaris
		if (count($horaris)==0) {
			return $this->render('horaris/list.html.twig', array(
				'message' => 'No hi ha horaris.',
				'horaris' => null,
			));
		}
		
		// Si hi ha horaris
		return $this->render('horaris/list.html.twig', array(
			'message' => null,
			'horaris' => $horaris,
		));
	}

    /**
     * @Route("/nou-horari/", name="creaHorariEstabliment")
     */
	public function getEstabliment(Request $request)
	{	
		$est = new Establiment();

		$form = $this->createFormBuilder($est)
			->add('nom', EntityType::class, array(
			'class' => 'AppBundle:Establiment',
			'choice_label' => 'nom',
			'choice_value' => 'id',
			'attr' => [
				'class' => 'form-control',
				],
			))
			->add('save', SubmitType::class, array('label' => 'Següent',
				'attr' => [
					'class' => 'btn btn-primary',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			return $this->redirectToRoute("creaHorariDepartament", array(
				'establiment'=>$form->get('nom')->getData()->getId()
			));
 		}

		return $this->render('horaris/form.html.twig', array(
			'title' => 'Creant horari',
            'form' => $form->createView(),
		));
	}

	/**
     * @Route("/nou-horari/{establiment}/", name="creaHorariDepartament")
     */
	public function getDepartament($establiment, Request $request)
	{
		$deps = $this->getDoctrine()
			->getManager()
			->createQuery('SELECT d.id, d.nom FROM AppBundle:Departament d WHERE d.establiment = ' . $establiment)
			->getResult();

		$arrDeps = array();
		for($i = 0; $i < count($deps); $i++) {
			$arrDeps[$deps[$i]['nom']] = $deps[$i]['id'];
		}

		$dep = new Departament();

		$form = $this->createFormBuilder($dep)
			->setAction($this->generateUrl('creaHorariDepartament', array('establiment'=>$establiment)))
			->add('establiment', ChoiceType::class, array(
					'choices' => $arrDeps,
					'label' => false,
					'attr' => [
						'class' => 'form-control',
						],
				))
			->add('save', SubmitType::class, array('label' => 'Següent',
				'attr' => [
					'class' => 'btn btn-primary',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$session = $this->get('session');
			$session->set('idDepartament', $form->get('establiment')->getData());
			return $this->redirectToRoute("creaHorariData", array(
				'establiment' => $establiment,
				'departament' => $session->get('idDepartament')
			));
		}

		return $this->render('horaris/form.html.twig', array(
			'title' => 'Creant horari',
            'form' => $form->createView(),
		));
	}

	/**
     * @Route("/nou-horari/{establiment}/{departament}/", name="creaHorariData")
     */
	public function getData($establiment, Request $request)
	{
		$session = $this->get('session');
		$days = new \DateTime('+7 days');

		$defaultData = array('message' => 'Test Data');

		$form = $this->createFormBuilder($defaultData)
			->add('date', DateType::class, array(
					'widget' => 'single_text', 
					'data'   => $days,
                   'attr'   => ['min' => $days->format('Y-m-d'),
								'class' => 'form-control pull-right',]))
			->add('save', SubmitType::class, array('label' => 'Següent',
				'attr' => [
					'class' => 'btn btn-primary',
					]))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			$dateTime = new \DateTime($data['date']->format('d-m-Y'));
			$weekNo = $dateTime->format('W');
			$newDate = new \DateTime();
			$newDate->setISODate($dateTime->format('Y'), $weekNo);
			$session = $this->get('session');
			$session->set('idData', $newDate->format('d-m-Y'));
			//return $this->redirectToRoute("infoTest");
			return $this->redirectToRoute("crearHorari", array(
				'establiment' => $establiment,
				'departament' => $session->get('idDepartament'),
				'data' => $session->get('idData'),
			));
		}

		return $this->render('horaris/form.html.twig', array(
			'title' => "Creant horari",
            'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/nou-horari/{establiment}/{departament}/{data}", name="crearHorari")
	 */
	public function insertAction(Request $request)
	{
		// Dades de sessió
		$session = $this->get('session');
		$departamentId = $session->get('idDepartament');
		$data = $session->get('idData');


		// Recupera l'id de l'últim horari creat
		$horariId = $this->getDoctrine()
			->getManager()
			->createQuery("SELECT MAX(h.id) FROM AppBundle:Horari h")
			->getResult();
		$novaId = $horariId[0][1] + 1;
		$session->set('idHorari', $novaId);

		$horari = new Horari();

		// Comprova si ja existeix un horari per el mateix departament i el mateix dia
		$qb = $this->getDoctrine()->getManager()->createQueryBuilder();
		$result = $qb->select('h.id')->from('AppBundle:Horari', 'h')
		->where('h.departament = :departament AND h.titol like :titol')
		->setParameters(array('departament' => $departamentId, 'titol' => $data.'%'))
		->getQuery()
		->getResult();

		// Si ja existeix un horari per el mateix departament la mateixa setmana, redirigeix a editar l'horari 
		if (!empty($result)) {
			return $this->redirectToRoute('editarHorari', array('id' => $result[0]['id']));
		}

		// Departament
		$departament = $this->getDoctrine()
			->getRepository(Departament::class)
			->findOneById($departamentId);

		// Establiment
		$establiment = $departament->getEstabliment();

		// Data
		$dia = substr($data,0,2);
		$mes = substr($data,3,2);
		$any = substr($data,6,4);

		// Calula data final
		$date = date("d-m-Y", strtotime($any."-".$mes."-".$dia));
		$dataFi = date('d-m-Y', strtotime('+6 day', strtotime($date)));

		// Titol
		$titol = $date . ' / ' . $dataFi;

		// Dades horari
		$horari->setId($novaId);
		$horari->setDepartament($departament);
		$horari->setAny($any);
		$horari->setMes($mes);
		$horari->setTitol($titol);

		// Form
		$form = $this->createFormBuilder($horari, array('allow_extra_fields' => true))
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save hidden'),
			))
			->getForm();

		// Empleats del departament
		$empleats = $this->getDoctrine()->getManager()
			->getRepository('AppBundle:Empleat')
			->createQueryBuilder('e')
			->innerJoin('e.departaments', 'd')
			->where('d = :departament')
			->setParameter('departament', $horari->getDepartament())
			->getQuery()
			->getResult();

		$form->handleRequest($request);

		// Formulari correctament enviat
		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($horari);
			$em->flush();
		}

		return $this->render('horaris/crear.html.twig', array(
			'empleats' => $empleats,
			'form' => $form->createView(),
			'dia' => $data,
			'horariTitol' => $titol,
			'nomEstabliment' => $establiment->getNom(),
			'nomDepartament' => $departament->getNom(),
			'horariId' => $novaId,
		));
	}

	/**
	 * @Route("/dia-empleat/{data}/{empleat}/", name="crearHorariDiaEmpleat")
	 */
	public function insertDiaEmpleatAction($data, $empleat, Request $request)
	{
		// Dades de sessió
		$session = $this->get('session');
		$departamentId = $session->get('idDepartament');
		$horariId = $session->get('idHorari');

		// Recupera l'últim horari creat
		$horari = $this->getDoctrine()->getRepository(Horari::class)
			->findOneById($horariId);

		// Dia
		$dia = substr($data, 0, 2);

		// Mes
		$mes = substr($data, 3, 2);

		// Any
		$any = substr($data, -4);

		// Dia de la setmana per comprovar si és cap de setmana
		$dSetmana = date("w", strtotime($any."-".$mes."-".$dia));

		// Horari setmana anterior
		$date = date("d-m-Y", strtotime($data));
		$setmana = date('W', strtotime($date));
		$setmanaAnterior = $setmana - 1;
		$primerDiaSetmana = date("d-m-Y", strtotime("{$any}-W{$setmanaAnterior}-1"));
		$diaContador = $dSetmana;
		if ($diaContador == 0) {
			$diaContador = 7;
		}
		$diaSetmanaAnterior = date("d", strtotime("{$any}-W{$setmanaAnterior}-$diaContador"));

		$query = $this->getDoctrine()->getManager()
			->createQuery('SELECT th.id FROM AppBundle:HorariEmpleat he JOIN AppBundle:Horari h WHERE h.id = he.horari JOIN AppBundle:TipusHora th WHERE he.tipusHora = th.id WHERE h.departament = :departament AND h.titol LIKE :titol AND he.dia = :dia AND he.empleat = :empleat')
			->setParameters(array(
				'departament' => $departamentId,
				'titol' => $primerDiaSetmana.'%',
				'dia' => $diaSetmanaAnterior,
				'empleat' => $empleat,
			));
		$horaAnterior = $query->getOneOrNullResult();



		$horariEmpleat = new HorariEmpleat();
		$horariEmpleat->setHorari($horari);
		$horariEmpleat->setDia($dia);
		$emp = $this->getDoctrine()->getRepository(Empleat::class)
			->findOneByDni($empleat);
		$horariEmpleat->setEmpleat($emp);

		// Treballa cap de setmana
		$finde = $emp->getTreballaCapSetmana();

		// Recupera les hores de l'empleat
		$hores = $this->getDoctrine()
			->getManager()
			->createQuery("SELECT th.id FROM AppBundle:HorariEmpleat he JOIN AppBundle:Horari h WHERE he.horari = h.id JOIN AppBundle:TipusHora th WHERE he.tipusHora = th.id WHERE he.empleat = '".$empleat."' AND  h.any = ".$any." AND h.mes = ".$mes." AND he.dia = ".$dia)
			->getResult();

		// Inicialitza els missatges per a l'usuari
		$messages = array();
		$findeCheck = false;

		$form = $this->createFormBuilder($horariEmpleat)
			->setAction($this->generateUrl('crearHorariDiaEmpleat', array(
				'data' => $data,
				'empleat' => $empleat,
			)))
			->add('tipusHora', EntityType::class, array(
				'class' => 'AppBundle:TipusHora',
				'choice_label' => 'descripcio',
				'label_attr' => array('class' => 'hidden'),
				'choice_attr' => function($val, $key, $index) use ($hores, $dSetmana, $finde, $horaAnterior, &$messages, &$findeCheck) {
					$selected = false;
					$disabled = false;
					$hidden = false;
					// Comprova l'horari de la setmana anterior
					if ($val->getId() == $horaAnterior['id']) {
						$selected = true;
					}
					// Comprova si l'empleat ja te hores ocupades aquell dia
					if (!empty($hores) && $val->getId() == $hores[0]['id'] && $val->getId() != 1) {
						//$disabled = true;
						$messages[] = "L'empleat ja treballa l'hora: " . $val->getDescripcio();
					}
					// Comprova si és cap de setmana i l'empleat treballa
					if (($dSetmana == 0 || $dSetmana == 6) && !$finde && $val->getId() != 1) {
						$hidden = true;
						if (!$findeCheck) {
							$messages[] = "L'empleat no treballa els caps de setmana";
							$findeCheck = true;
						}
					}
					return array(
						'disabled' => $disabled,
						'selected' => $selected,
						'hidden' => $hidden,
					);
				},
				'attr' => [
					'class' => 'form-control',
				]
			))
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save hidden')
			))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($horariEmpleat);
			$em->flush();
		}
		
		return $this->render('horaris/hora.html.twig', array(
			'form' => $form->createView(),
			'messages' => $messages,
		));
	}

	/**
	 * @Route("/editar-horari/{id}", name="editarHorari")
	 */
	public function editAction($id, Request $request)
	{
		// Es recupera l'horari a editar
		$em = $this->getDoctrine()->getManager();
		$horari = $em->getRepository('AppBundle:Horari')
			->findOneById($id);

		// Data inicial
		$primerDia = substr($horari->getTitol(), 0, 10);

		// Es recupera el departament
		$departament = $horari->getDepartament();

		// Es recupera l'establiment
		$establiment = $departament->getEstabliment();

		// Empleats del departament
		$empleats = $this->getDoctrine()->getManager()
			->getRepository('AppBundle:Empleat')
			->createQueryBuilder('e')
			->innerJoin('e.departaments', 'd')
			->where('d = :departament')
			->setParameter('departament', $horari->getDepartament())
			->getQuery()
			->getResult();

		// Es crea el formulari
		$form = $this->createFormBuilder($horari, array('allow_extra_fields' => true))
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save hidden')
			))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
		}

		return $this->render('horaris/editar.html.twig', array(
			'empleats' => $empleats,
			'form' => $form->createView(),
			'dia' => $primerDia,
			'horari' => $id,
			'horariTitol' => $horari->getTitol(),
			'nomEstabliment' => $establiment->getNom(),
			'nomDepartament' => $departament->getNom(),
		));
	}

	/**
	 * @Route("/editar-dia-empleat/{horari}/{data}/{empleat}/", name="editarHorariDiaEmpleat")
	 */
	public function editDiaEmpleatAction($horari, $data, $empleat, Request $request)
	{

		// Dia
		$dia = substr($data, 0, 2);

		// Mes
		$mes = substr($data, 3, 2);

		// Any
		$any = substr($data, -4);

		// Dia de la setmana per comprovar si és cap de setmana
		$dSetmana = date("w", strtotime($any."-".$mes."-".$dia));

		// Treballa cap de setmana
		$emp = $this->getDoctrine()->getRepository(Empleat::class)
			->findOneByDni($empleat);
		$finde = $emp->getTreballaCapSetmana();

		// Recupera les hores de l'empleat
		$hores = $this->getDoctrine()
			->getManager()
			->createQuery("SELECT th.id FROM AppBundle:HorariEmpleat he JOIN AppBundle:Horari h WHERE he.horari = h.id JOIN AppBundle:TipusHora th WHERE he.tipusHora = th.id WHERE he.empleat = '".$empleat."' AND  h.any = ".$any." AND h.mes = ".$mes." AND he.dia = ".$dia." AND he.horari != ".$horari)
			->getResult();
		
		// Es recuperen les dades ja creades
		$em = $this->getDoctrine()->getManager();
		$horariEmpleat = $em->getRepository(HorariEmpleat::class)
			->findOneBy(array(
				'horari' => $horari,
				'empleat' => $empleat,
				'dia' => $dia,
			));

		// Inicialitza els missatges per a l'usuari
		$messages = array();

		// Es crea el formulari
		$form = $this->createFormBuilder($horariEmpleat)
			->setAction($this->generateUrl('editarHorariDiaEmpleat', array(
				'horari' => $horari,
				'data' => $data,
				'empleat' => $empleat,
			)))
			->add('tipusHora', EntityType::class, array(
				'class' => 'AppBundle:TipusHora',
				'choice_label' => 'descripcio',
				'label_attr' => array('class' => 'hidden'),
				'attr' => [
					'class' => 'form-control',
					],
				'choice_attr' => function($val, $key, $index) use ($hores, $dSetmana, $finde, &$messages, &$findeCheck) {
					$hidden = false;
					$disabled = false;
					// Comprova si l'empleat ja te hores ocupades aquell dia
					if (!empty($hores) && $val->getId() == $hores[0]['id'] && $val->getId() != 1) {
						//$disabled = true;
						$messages[] = "L'empleat ja treballa l'hora: " . $val->getDescripcio();
					}
					// Comprova si és cap de setmana i l'empleat treballa
					if (($dSetmana == 0 || $dSetmana == 6) && !$finde && $val->getId() != 1) {
						$hidden = true;
						if (!$findeCheck) {
							$messages[] = "L'empleat no treballa els caps de setmana";
							$findeCheck = true;
						}
					}
					return array(
						'disabled' => $disabled,
						'hidden' => $hidden,
					);
				},
			))
			->add('save', SubmitType::class, array(
				'attr' => array('class' => 'save hidden')
			))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
		}
		
		return $this->render('horaris/hora.html.twig', array(
			'form' => $form->createView(),
			'messages' => $messages,
		));
	}

	/**
	 * @Route("/veure-horari/{id}", name="veureHorari")
	 */
	public function viewAction($id, Request $request)
	{
		// Es recupera l'horari
		$em = $this->getDoctrine()->getManager();
		$horari = $em->getRepository('AppBundle:Horari')
			->findOneById($id);

		// Data inicial
		$primerDia = substr($horari->getTitol(), 0, 10);

		// Es recupera el departament
		$departament = $horari->getDepartament();

		// Es recupera l'establiment
		$establiment = $departament->getEstabliment();

		// Es recuperen els empleats del departament
		$empleats = $this->getDoctrine()->getManager()
			->getRepository('AppBundle:Empleat')
			->createQueryBuilder('e')
			->innerJoin('e.departaments', 'd')
			->where('d = :departament')
			->setParameter('departament', $horari->getDepartament())
			->getQuery()
			->getResult();

		return $this->render('horaris/veure.html.twig', array(
			'empleats' => $empleats,
			'dia' => $primerDia,
			'horariId' => $id,
			'horariTitol' => $horari->getTitol(),
			'nomEstabliment' => $establiment->getNom(),
			'nomDepartament' => $departament->getNom(),
		));
	}

	/**
	 * @Route("/veure-dia-empleat/{horari}/{dia}/{empleat}/", name="veureHorariDiaEmpleat")
	 */
	public function viewDiaEmpleatAction($horari, $dia, $empleat, Request $request)
	{
		// Es recuperen les dades ja creades
		$em = $this->getDoctrine()->getManager();
		$hora = $em->createQuery("SELECT th.descripcio FROM AppBundle:HorariEmpleat he JOIN AppBundle:TipusHora th WHERE he.tipusHora = th.id WHERE he.horari = '" . $horari . "' AND he.empleat = '" . $empleat . "' AND he.dia = " . $dia)
			->getResult();

		if (empty($hora)) {
			//$hora = $em->getRepository('AppBundle:TipusHora')->findById(1);
			$hora = $em->createQuery("SELECT th.descripcio FROM AppBundle:TipusHora th WHERE th.id = 1")->getResult();
		}

		return $this->render('horaris/veure-hora.html.twig', array(
			'hora' => $hora[0]['descripcio'],
		));
	}

	/**
	 * @Route("/imprimir-horari/{id}", name="imprimirHorari")
	 */
	public function printHorari($id, Request $request)
	{
		return $this->redirectToRoute('veureHorari', array('id' => $id, 'action' => 'print'));
	}
}

