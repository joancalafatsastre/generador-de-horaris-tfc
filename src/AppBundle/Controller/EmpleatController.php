<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Empleat;

class EmpleatController extends Controller
{
	/**
	 * @Route("/empleats/", name="empleats")
	 */
	public function listAction(Request $request)
	{

	
		// Recupera tots els empleats
		$empleats = $this->getDoctrine()
			->getRepository('AppBundle:Empleat')
			->findAll();

		$departaments = $this->getDoctrine()
		->getRepository('AppBundle:Departament')
		->findAll();

		// Si no hi ha empleats
		if (count($empleats)==0) {
			return $this->render('empleats/list.html.twig', array(
				'title' => 'Empleats',
				'empleats' => null,
			));
		}
		$em = $this->getDoctrine()->getManager();
		$connection = $em->getConnection();
		$statement = $connection->prepare("SELECT id FROM Departament WHERE activat = 0");
		$statement->execute();
		$results = $statement->fetchAll();


		$allData = array();
		$departaments_desactivats=0;
		foreach($empleats as $empleat) {

			$deps = array();
			$dni = $empleat->getDni();
			$nom = $empleat->getNom();
			$cognom1 = $empleat->getCognom1();
			$cognom2 = $empleat->getCognom2();
			$treb = $empleat->getTreballaCapSetmana();

			$activat = $empleat->getactivat();


			if($activat!=false){

			$allData[] = array(
					'dni' => $dni,
					'nom' => $nom,
					'cognom1' => $cognom1,
					'cognom2' => $cognom2,
					'treballaCapSetmana' => $treb,
					'activat' => $activat,
					'arrayDeps' => $empleat->getDepartaments(),
			);
		}

	}

		// Si hi ha empleats
		return $this->render('empleats/list.html.twig', array(
			'title' => 'Empleats',
			'empleats' => $allData,
		));


	}

	/**
	 * @Route("/nou-empleat/", name="creaEmpleat")
	 */
	public function insertAction(Request $request)
	{
		$empleat = new Empleat();

		$form = $this->createFormBuilder($empleat)
			->add('dni', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('cognom1', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('cognom2', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('treballacapsetmana', CheckboxType::class, array(
				'label' => 'Treballa el cap de setmana',
				'required' => false,
				'attr' => [
					'class' => 'checkbox',
					]
			))
			->add('departaments', EntityType::class, array(
				'class' => 'AppBundle:Departament',
				'choice_label' => function ($departaments) {
					$nomEst = $departaments->getEstabliment()->getNom();
					return $departaments->getNom()." - ".$nomEst;
				},
				'required' => true,
				'multiple' => true,
				'attr' => [
					'class' => 'form-control',
					'multiple' => '',
					]
			))
			->add('save', SubmitType::class, array('label' => 'Crea',
				'attr' => [
					'class' => 'btn btn-primary',
					'multiple' => '',
					]
			))
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($empleat);
			$em->flush();
			return $this->redirectToRoute('empleats');
		}

		return $this->render('empleats/form.html.twig', array(
			'message' => null,
			'title' => 'Crear Empleat',
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/editar-empleat/{dni}", name="editarEmpleat")
	 */
	public function editAction($dni, Request $request)
	{
		// Es recupera l'empleat a editar
		$em = $this->getDoctrine()->getManager();
		$establiment = $em->getRepository('AppBundle:Empleat')
			->findOneByDni($dni);
		$departaments = $em->getRepository('AppBundle:Departament')
			->findAll();

		// Es crea el formulari
		$form = $this->createFormBuilder($establiment)
			->add('dni', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					'readonly' => '',
					]
				])
			->add('nom', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('cognom1', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('cognom2', TextType::class, [
                'attr' => [
					'class' => 'form-control',
					]
				])
			->add('treballacapsetmana', CheckboxType::class, array(
				'label' => 'Treballa el cap de setmana',
				'required' => false,
				'attr' => [
					'class' => 'checkbox',
					]
			))
			->add('departaments', EntityType::class, array(
				'class' => 'AppBundle:Departament',
				'choice_label' => function ($departaments) {
					$nomEst = $departaments->getEstabliment()->getNom();
					return $departaments->getNom()." - ".$nomEst;
				},
				'required' => true,
        		'multiple' => true,
				'attr' => [
					'class' => 'form_control',
					'multiple' => '',
					]
			))
			->add('save', SubmitType::class, array('label' => 'Editar',
				'attr' => [
					'class' => 'btn btn-primary',
					'multiple' => '',
					]))

			->getForm();
		
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em->flush();
			return $this->redirectToRoute('empleats');
		}

		return $this->render('empleats/form.html.twig', array(
			'title' => 'Editar Empleat',
			'form' => $form->createView(),
		));
	}

	/**
	 * @Route("/eliminar-empleat/{dni}", name="eliminarEmpleat")
	 */
	public function removeAction($dni, Request $request)
	{
		
	}

	/**
	 * @Route("/importarEmpleat", name="importarEmpleat")
	 */
	public function importAction()
	{
		return $this->render('empleats/import.html.twig', array(
			
		));
	}

	/**
	 * @Route("/deshabilitarEmpleat/{dni}", name="deshabilitarEmpleat")
	 */
	public function deshabilitarAction($dni)
	{
    $entityManager = $this->getDoctrine()->getManager();
    $empleat = $entityManager->getRepository(Empleat::class)->find($dni);

    if (!$empleat) {
        throw $this->createNotFoundException(
            "No s'ha trobat cap empleat amb el dni".$dni
        );
    }
    $valorActivat= $empleat->getactivat();
    if($valorActivat==true){
    	$empleat->setactivat(0);
    	$entityManager->flush();
    }else{
    	$empleat->setactivat(1);
    	$entityManager->flush();
    }
    return $this->redirectToRoute('empleats');
}
	
/**
	 * @Route("/enviar", name="enviar")
	 */public function indexAction(Request $request)
{
		/*$link = mysqli_connect('localhost', 'root', 'ausias', 'symfony');
mysqli_select_db($link, 'Empleats');*/
		$em = $this->getDoctrine()->getManager();
		//Aquí es donde seleccionamos nuestro csv
         $fname = $_FILES['sel_file']['name'];
         echo 'Carregant el fitxer '.$fname.'... <br>';
         $chk_ext = explode(".",$fname);
 
         if(strtolower(end($chk_ext)) == "csv")
         {
             //si es correcto, entonces damos permisos de lectura para subir
             $filename = $_FILES['sel_file']['tmp_name'];
             $handle = fopen($filename, "r");
             $error = 0;
             while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
             {
               //Insertamos los datos con los valores...
              
                $empleat = new Empleat();
                $empleat->setDni($data[0]);
                $empleat->setNom($data[1]);
                $empleat->setCognom1($data[2]);
                $empleat->setCognom2($data[3]);
                $empleat->setTreballaCapSetmana($data[4]);

		        $em->persist($empleat);
		        $em->flush();
             }
             //cerramos la lectura del archivo "abrir archivo" con un "cerrar archivo"
             fclose($handle);
             echo "Usuaris creats a la base de dades";
             ?>
             <form action="/empleats">
                <input type="submit" value="Tornar als empleats" />
            </form>

             <?php
         }
         else
         {
            //si aparece esto es posible que el archivo no tenga el formato adecuado, inclusive cuando es cvs, revisarlo para             
        //ver si esta separado por " , "
			         }
			         return $this->redirectToRoute('empleats');
}

}
