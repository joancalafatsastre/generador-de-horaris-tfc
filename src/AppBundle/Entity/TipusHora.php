<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipusHora
 *
 * @ORM\Table(name="TipusHora")
 * @ORM\Entity
 */
class TipusHora
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="descripcio", type="string", length=50, nullable=false)
     */
    private $descripcio;

    public function getId()
    {
        return $this->id;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        $this->descripcio = $descripcio;
    }
}
