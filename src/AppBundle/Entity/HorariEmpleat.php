<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="HorariEmpleat")
 * @ORM\Entity
 */
class HorariEmpleat
{

    /**
     * @ORM\ManyToOne(targetEntity="Horari")
     * @ORM\JoinColumn(name="horari", referencedColumnName="id")
     * @ORM\Id
     */
    private $horari;

    /**
     * @ORM\ManyToOne(targetEntity="Empleat")
     * @ORM\JoinColumn(name="empleat", referencedColumnName="dni")
     * @ORM\Id
     */
    private $empleat;

    /**
     * @ORM\Column(name="dia", type="float")
     * @ORM\Id
     */
    private $dia;

    /**
     * @ORM\ManyToOne(targetEntity="TipusHora")
     * @ORM\JoinColumn(name="hora", referencedColumnName="id")
     */
    private $tipusHora;

    public function getHorari()
    {
        return $this->horari;
    }

    public function setHorari($horari)
    {
        $this->horari = $horari;
    }

    public function getEmpleat()
    {
        return $this->empleat;
    }

    public function setEmpleat($empleat)
    {
        $this->empleat = $empleat;
    }

    public function getDia()
    {
        return $this->dia;
    }

    public function setDia($dia)
    {
        $this->dia = $dia;
    }

    public function getTipusHora()
    {
        return $this->tipusHora;
    }

    public function setTipusHora($tipusHora)
    {
        $this->tipusHora = $tipusHora;
    }

    public function __toString()
	{
	    return $this->horari.''.$this->empleat;
	}
}
