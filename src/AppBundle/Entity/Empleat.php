<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Empleat")
 * @ORM\Entity
 */
class Empleat
{

    /**
     * @ORM\Column(name="dni", type="string", length=9)
     * @ORM\Id
     */
    private $dni;

    /**
     * @ORM\Column(name="nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @ORM\Column(name="cognom1", type="string", length=50, nullable=false)
     */
    private $cognom1;

    /**
     * @ORM\Column(name="cognom2", type="string", length=50, nullable=false)
     */
    private $cognom2;

    /**
     * @ORM\Column(name="treballa_cap_setmana", type="boolean", nullable=false, options={"default" : 0})
     */
    private $treballaCapSetmana;

    /**
     * @ORM\ManyToMany(targetEntity="Departament", inversedBy="empleats")
     * @ORM\JoinTable(name="EmpleatDepartament", joinColumns={@ORM\JoinColumn(name="empleat", referencedColumnName="dni")}, inverseJoinColumns={@ORM\JoinColumn(name="departament", referencedColumnName="id")})
     */
    private $departaments;

    /**
     * @ORM\Column(name="activat", type="boolean", nullable=false, options={"default" : 1})
     */
    private $activat;

    public function __construct()
    {
        $this->departaments = new \Doctrine\Common\Collections\ArrayCollection();
		$this->activat = 1;
    }

    public function getDni()
    {
        return $this->dni;
    }

    public function setDni($dni)
    {
       $this->dni = $dni;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getCognom1()
    {
        return $this->cognom1;
    }

    public function setCognom1($cognom1)
    {
        $this->cognom1 = $cognom1;
    }

    public function getCognom2()
    {
        return $this->cognom2;
    }

    public function setCognom2($cognom2)
    {
        $this->cognom2 = $cognom2;
    }

    public function getTreballaCapSetmana()
    {
        return $this->treballaCapSetmana;
    }

    public function setTreballaCapSetmana($treballaCapSetmana)
    {
        $this->treballaCapSetmana = $treballaCapSetmana;
    }

    public function getDepartaments()
    {
        return $this->departaments;
    }

    public function setDepartaments($departaments)
    {
        $this->departaments = $departaments;
    }

    public function getactivat()
    {
        return $this->activat;
    }

    public function setactivat($activat)
    {
        $this->activat = $activat;
    }
}

