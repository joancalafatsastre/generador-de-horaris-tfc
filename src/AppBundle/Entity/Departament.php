<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departament
 *
 * @ORM\Table(name="Departament")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartamentRepository")
 */
class Departament
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="nom", type="string", length=150, nullable=false)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="Establiment")
     * @ORM\JoinColumn(name="establiment", referencedColumnName="id")
     */
    private $establiment;

    /**
     * @ORM\Column(name="activat", type="boolean", nullable=false, options={"default" : 1})
     */
    private $activat;

    /**
     * @ORM\ManyToMany(targetEntity="Empleat", mappedBy="departaments")
     */ 
    private $empleats;

    public function __construct()
    {
        $this->empleats = new \Doctrine\Common\Collections\ArrayCollection();
		$this->activat = 1;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getEstabliment()
    {
        return $this->establiment;
    }

    public function setEstabliment($establiment)
    {
        $this->establiment = $establiment;
    }

    public function getEmpleats()
    {
        return $this->empleats;
    }

    public function setEmpleats($empleats)
    {
        $this->empleats = $empleats;
    }

    public function getactivat()
    {
        return $this->activat;
    }

    public function setactivat($activat)
    {
        $this->activat = $activat;
    }

    public function __toString()
    {
        return  $this->nom;
    }

}

