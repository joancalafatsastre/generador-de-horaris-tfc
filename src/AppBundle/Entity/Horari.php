<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Horari")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HorariRepository")
 */
class Horari
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Departament")
     * @ORM\JoinColumn(name="departament", referencedColumnName="id")
     */
    private $departament;

    /**
     * @ORM\Column(name="any", type="float", precision=4, scale=0, nullable=false)
     */
    private $any;

    /**
     * @ORM\Column(name="mes", type="float", precision=2, scale=0, nullable=false)
     */
    private $mes;

    /**
     * @ORM\Column(name="titol", type="string", length=150, nullable=false)
     */
    private $titol;

    public function getId()
    {
        return $this->id;
    }

	public function setId($id)
	{
		$this->id = $id;
	}

    public function getDepartament()
    {
        return $this->departament;
    }

    public function setDepartament($departament)
    {
        $this->departament = $departament;
    }

    public function getAny()
    {
        return $this->any;
    }

    public function setAny($any)
    {
        $this->any = $any;
    }

    public function getMes()
    {
        return $this->mes;
    }

    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    public function getTitol()
    {
        return $this->titol;
    }

    public function setTitol($titol)
    {
        $this->titol = $titol;
    }
}