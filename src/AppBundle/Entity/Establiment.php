<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Establiment
 *
 * @ORM\Table(name="Establiment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EstablimentRepository")
 */
class Establiment
{

	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\Column(name="nom", type="string", length=150, nullable=false)
	 */
	private $nom;

	 /**
     * @ORM\Column(name="activat", type="boolean", nullable=false, options={"default" : 1})
     */
    private $activat;

	public function __construct()
	{
		$this->activat = 1;
	}

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getNom()
	{
		return $this->nom;
	}

	public function setNom($nom)
	{
		$this->nom = $nom;
	}


	public function __toString()
	{
	    return $this->nom;
	}

    public function setActivat($activat)
    {
        $this->activat = $activat;

        return $this;
    }

    public function getActivat()
    {
        return $this->activat;
    }
}
